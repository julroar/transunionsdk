//
//  OnfidoNumbers.swift
//  onfidoTest
//
//  Created by USER on 7/15/19.
//  Copyright © 2019 david. All rights reserved.
//

import Foundation

struct OnfidoNumbers:Codable {
    var type:String?
    var value:String?
    var stateCode:String?
    
    enum CodingKeys: String, CodingKey {
        case type = "type"
        case value = "value"
        case stateCode = "state_code"
    }
    
    public init(from decoder: Decoder) throws {
        
        let numberValues = try decoder.container(keyedBy: CodingKeys.self)
        type = try numberValues.decodeIfPresent(String.self, forKey: .type) ?? ""
        value = try numberValues.decodeIfPresent(String.self, forKey: .value) ?? ""
        stateCode = try numberValues.decodeIfPresent(String.self, forKey: .stateCode) ?? ""
    }
}
