//
//  TransunionViewController.swift
//  TransUnionSDK
//
//  Created by Julian Arredondo on 7/14/19.
//  Copyright © 2019 Julian Arredondo. All rights reserved.
//

import UIKit
import Onfido
import FraudForce

public protocol TransunionDelegate: class {
    func startSuccess(_ success: Bool?)
}

public class TransunionViewController: UIViewController {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var lastnameTexfield: UITextField!
    
 public weak var delegate: TransunionCallback?
   public init() {
            let bundle = Bundle(for: TransunionViewController.self)
        super.init(nibName: "TransunionViewController", bundle: bundle)
        readConfigFile()
        FraudForce.start()
    }
    
    func readConfigFile(){
        if let path = Bundle.main.path(forResource: pList.Info.rawValue, ofType: "plist"), let dict = NSDictionary(contentsOfFile: path) as? [String: AnyObject] {
            var serverType:ServerType
            
            #if DEVELOPMENT
            serverType = .development
            #elseif DEBUGDEVELOPMENT
            serverType = .development
            #elseif TEST
            serverType = .test
            #elseif DEBUGTEST
            serverType = .test
            #elseif PRODUCTION
            serverType = .production
            #elseif DEBUGPRODUCTION
            serverType = .production
            #else
            serverType = .development
            #endif
            
            switch serverType {
                
            case ServerType.development:
                Configuration.tokenSDK = "test_jIqN83WvAm8coBEVSo1LZr5A1P-mIcuh"
                Configuration.nameTransUnion = "OruggaUser"
                Configuration.lastNameTransunion = "W3lc0me5pring@2019"
                Configuration.urlBaseOnfido = "https://api.onfido.com"
                Configuration.urlBaseTransUnion = "https://api.onfido.com"
                if let path = Bundle.main.path(forResource: pList.ConfigDevelopment.rawValue, ofType: "plist"), let dict = NSDictionary(contentsOfFile: path) as? [String: AnyObject] {
                    
                    setConfiguration(dict: dict)
                }
                
                break
            case ServerType.test:
                if let path = Bundle.main.path(forResource: pList.ConfigTest.rawValue, ofType: "plist"), let dict = NSDictionary(contentsOfFile: path) as? [String: AnyObject] {
                    setConfiguration(dict: dict)
                }
                break
            case ServerType.production:
                if let path = Bundle.main.path(forResource: pList.ConfigProduction.rawValue, ofType: "plist"), let dict = NSDictionary(contentsOfFile: path) as? [String: AnyObject] {
                    setConfiguration(dict: dict)
                }
                
                break
            default:
                
                if let path = Bundle.main.path(forResource: pList.ConfigDevelopment.rawValue, ofType: "plist"), let dict = NSDictionary(contentsOfFile: path) as? [String: AnyObject] {
                    
                    setConfiguration(dict: dict)
                }
                
                break
            }
        }
    }
    
    func setConfiguration(dict:[String: AnyObject]){
        Configuration.tokenSDK = dict[configurationKeys.tokenSDK.rawValue] as! String
        Configuration.nameTransUnion = dict[configurationKeys.nameTransUnion.rawValue] as! String
        Configuration.lastNameTransunion = dict[configurationKeys.lastNameTransunion.rawValue] as! String
        Configuration.urlBaseOnfido = dict[configurationKeys.urlBaseOnfido.rawValue] as! String
        Configuration.urlBaseTransUnion = dict[configurationKeys.urlBaseTransUnion.rawValue] as! String
    }

    @IBAction func action(_ sender: Any) {
        validateTexfields()
        return
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        EncoreTransunion.sharedInstance().createController(callback: delegate!)
    }
    
    func validateTexfields() {
        if nameTextField.text!.isEmpty || lastnameTexfield.text!.isEmpty {
            return
        } else {
            createAplicant(firstName: nameTextField.text ?? "", lastName: lastnameTexfield.text ?? "")
        }
    }
    
    func createAplicant(firstName: String, lastName: String) {
        let onfidoApplicants = OnfidoApplicants(firstNames: firstName, lastNames: lastName)
        APIOnfido.createApplicant(applicant: onfidoApplicants) { (response, result) in
            switch result {
            case .success( let success):
                print(success)
                let config = try! OnfidoConfig.builder()
                    .withToken("test_vgcxgPfL9VY90ofwucKpGECvUIkILgk-")// estatico para el proyecto
                    .withApplicantId(success.id ?? "")// Dinamico por solicitud
                    .withDocumentStep(ofType: .nationalIdentityCard, andCountryCode: "COL")
                    .withFaceStep(ofVariant: .photo(withConfiguration: nil))
                    .build()
                
                let responseHandler: (OnfidoResponse) -> Void = { response in
                    switch response {
                    case let .error(error):
                        print(error)
                    case let .success(results):
                        let document: Optional<OnfidoResult> = results.filter({ result in
                            if case OnfidoResult.document = result { return true }
                            return false
                        }).first

                        if let documentUnwrapped = document, case OnfidoResult.document(let documentResponse) = documentUnwrapped {
                            print(documentResponse.id)
                        }
                        
                    case .cancel:
                        
                        print("cancel for user")
                        
                    }

                }
                
                let onfidoFlow = OnfidoFlow(withConfiguration: config)
                    
                    .with(responseHandler: responseHandler)
                let onfidoRun = try! onfidoFlow.run()
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
                    self.present(onfidoRun, animated: false, completion: nil)
                }
               
                break
            case .failure(_):
                break
            }
            
        }
    }
}

extension TransunionViewController: TransunionCallback {
    public func controllerCreated(controller: Bool) {
        
    }
    
    public func flowFinished(response: Bool) {
        
    }
    
   
}
