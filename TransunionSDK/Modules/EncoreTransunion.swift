//
//  EncoreTransunion.swift
//  TransUnionSDK
//
//  Created by Julian Arredondo on 7/14/19.
//  Copyright © 2019 Julian Arredondo. All rights reserved.
//

import Foundation


@objc public protocol TransunionCallback {
    @objc func controllerCreated(controller: Bool)
    @objc func flowFinished(response: Bool)
}

@objc @objcMembers  public class EncoreTransunion: NSObject {
    
    @objc  static let shared = EncoreTransunion()
    
    override init(){}
    
    @objc   public class func sharedInstance() -> EncoreTransunion {
        return EncoreTransunion.shared
    }
    
    @objc var delegateCallback: TransunionCallback!
    
    @objc public func createController(callback: TransunionCallback) {
        self.delegateCallback = callback
        let formRegister = PersonalInformationFormViewController()
            delegateCallback.controllerCreated(controller: true)
    }
}
