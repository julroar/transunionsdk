//
//  PersonalInformationFormViewController.swift
//  TransUnionSDK
//
//  Created by Julian Arredondo on 7/14/19.
//  Copyright © 2019 Julian Arredondo. All rights reserved.
//

import UIKit

class PersonalInformationFormViewController: UIViewController {

    init() {
        super.init(nibName: "PersonalInformationFormViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
