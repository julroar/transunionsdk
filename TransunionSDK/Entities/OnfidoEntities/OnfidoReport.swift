//
//  OnfidoReport.swift
//  onfidoTest
//
//  Created by USER on 7/13/19.
//  Copyright © 2019 david. All rights reserved.
//

import Foundation

struct OnfidoReport:Codable{
    
    var name:String?
    var variant:String?
    
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case variant = "variant"
    }
    
    init(){
        
    }
    
    func encode(to encoder: Encoder) throws{
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(name, forKey: .name)
        try container.encodeIfPresent(variant, forKey: .variant)
    }
}
