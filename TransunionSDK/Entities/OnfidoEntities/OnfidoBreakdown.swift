//
//  OnfidoBreakdown.swift
//  onfidoTest
//
//  Created by USER on 7/15/19.
//  Copyright © 2019 david. All rights reserved.
//

import Foundation

struct OnfidoBreakdown:Codable{
    var faceComparison:OnfidoResponseResult?
    var imageIntegrity:OnfidoResponseResult?
    var visualAuthenticity:OnfidoResponseResult?
    var ageValidation:OnfidoResponseResult?
    var compromisedDocument:OnfidoResponseResult?
    var dataComparison:OnfidoResponseResult?
    var dataConsistency:OnfidoResponseResult?
    var dataValidation:OnfidoResponseResult?
    var policeRecord:OnfidoResponseResult?
    
    enum codingKeys:String, CodingKey{
        case faceComparison = "face_comparison"
        case imageIntegrity = "image_integrity"
        case visualAuthenticity = "visual_authenticity"
        case ageValidation = "age_validation"
        case compromisedDocument = "compromised_document"
        case dataComparison = "data_comparison"
        case dataConsistency = "data_consistency"
        case dataValidation = "data_validation"
        case policeRecord = "police_record"
    }
    
    public init(from decoder:Decoder) throws{
        let breackdownValues = try decoder.container(keyedBy: codingKeys.self)
        faceComparison = try breackdownValues.decodeIfPresent(OnfidoResponseResult.self, forKey: .faceComparison)
        imageIntegrity = try breackdownValues.decodeIfPresent(OnfidoResponseResult.self, forKey: .imageIntegrity)
        visualAuthenticity = try breackdownValues.decodeIfPresent(OnfidoResponseResult.self, forKey: .visualAuthenticity)
        ageValidation = try breackdownValues.decodeIfPresent(OnfidoResponseResult.self, forKey: .ageValidation)
        compromisedDocument = try breackdownValues.decodeIfPresent(OnfidoResponseResult.self, forKey: .compromisedDocument)
        dataComparison = try breackdownValues.decodeIfPresent(OnfidoResponseResult.self, forKey: .dataComparison)
        dataConsistency = try breackdownValues.decodeIfPresent(OnfidoResponseResult.self, forKey: .dataConsistency)
        dataValidation = try breackdownValues.decodeIfPresent(OnfidoResponseResult.self, forKey: .dataValidation)
        policeRecord = try breackdownValues.decodeIfPresent(OnfidoResponseResult.self, forKey: .policeRecord)
    }
    
}
