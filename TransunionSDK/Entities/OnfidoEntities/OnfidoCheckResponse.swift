//
//  OnfidoCheckResponse.swift
//  onfidoTest
//
//  Created by USER on 7/15/19.
//  Copyright © 2019 david. All rights reserved.
//

import Foundation

struct OnfidoCheckResponse:Codable{
    var id:String?
    var createdAt:String?
    var status:String?
    var redirectUri:String?
    var type:String?
    var result:String?
    var sandbox:Bool?
    var resultsUri:String?
    var downloadUri:String?
    var formUri:String?
    var href:String?
    var paused:Bool?
    var version:String?
    var reportTypeGroups:[String]?
    var tags:[String]?
    var reports:[OnfidoReports]?
}
