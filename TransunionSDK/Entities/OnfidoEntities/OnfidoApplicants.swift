//
//  OnfidoApplicants.swift
//  onfidoTest
//
//  Created by USER on 7/13/19.
//  Copyright © 2019 david. All rights reserved.
//

import Foundation

struct OnfidoApplicants: Codable{
    
    var id:String?
    var createdAt:String?
    var sandbox:Bool?
    var title:String?
    var middleName:String?
    var firstName:String?
    var lastName:String?
    var email:String?
    var dob:String?
    var country:String?
    var deleteAt:String?
    var href:String?
    var idNumbers:[OnfidoNumbers]?
    var gender:String?
    var telephone:String?
    var mobile:String?
    var nationality:String?
    var mothersMaidenName:String?
    var countryOfBirth:String?
    var townOfBirth:String?
    var previousLastName:String?
    var addresses:[OnfidoAddress]?
    
    static var firstName:String?
    static var lastName:String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case createdAt = "created_at"
        case sandbox = "sandbox"
        case title = "title"
        case firstName = "first_name"
        case middleName = "middle_name"
        case lastName = "last_name"
        case email = "email"
        case dob = "dob"
        case country = "country"
        case deleteAt = "delete_at"
        case href = "href"
        case idNumbers = "id_numbers"
        case gender = "gender"
        case telephone = "telephone"
        case mobile = "mobile"
        case nationality = "nationality"
        case mothersMaidenName = "mothers_maiden_name"
        case countryOfBirth = "country_of_birth"
        case townOfBirth = "town_of_birth"
        case previousLastName = "previous_last_name"
        case addresses = "addresses"
    }
    
    func encode(to encoder: Encoder) throws{
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(firstName, forKey: .firstName)
        try container.encodeIfPresent(lastName, forKey: .lastName)
        
    }
    public init(firstNames: String, lastNames: String) {
        self.firstName = firstNames
        self.lastName = lastNames
    }
    
    public init(from decoder: Decoder) throws {
        
        let applicantsValues = try decoder.container(keyedBy: CodingKeys.self)
        id = try applicantsValues.decodeIfPresent(String.self, forKey: .id) ?? ""
        createdAt = try applicantsValues.decodeIfPresent(String.self, forKey: .createdAt) ?? ""
        sandbox = try applicantsValues.decodeIfPresent(Bool.self, forKey: .sandbox) ?? true
        title = try applicantsValues.decodeIfPresent(String.self, forKey: .title) ?? ""
        firstName = try applicantsValues.decodeIfPresent(String.self, forKey: .firstName) ?? ""
        middleName = try applicantsValues.decodeIfPresent(String.self, forKey: .middleName) ?? ""
        lastName = try applicantsValues.decodeIfPresent(String.self, forKey: .lastName) ?? ""
        email = try applicantsValues.decodeIfPresent(String.self, forKey: .email) ?? ""
        dob = try applicantsValues.decodeIfPresent(String.self, forKey: .dob) ?? ""
        country = try applicantsValues.decodeIfPresent(String.self, forKey: .country) ?? ""
        deleteAt = try applicantsValues.decodeIfPresent(String.self, forKey: .deleteAt) ?? ""
        href = try applicantsValues.decodeIfPresent(String.self, forKey: .href) ?? ""
        idNumbers = try applicantsValues.decodeIfPresent([OnfidoNumbers].self, forKey: .idNumbers) ?? []
        gender = try applicantsValues.decodeIfPresent(String.self, forKey: .gender) ?? ""
        telephone = try applicantsValues.decodeIfPresent(String.self, forKey: .telephone) ?? ""
        mobile = try applicantsValues.decodeIfPresent(String.self, forKey: .mobile) ?? ""
        nationality = try applicantsValues.decodeIfPresent(String.self, forKey: .nationality) ?? ""
        mothersMaidenName = try applicantsValues.decodeIfPresent(String.self, forKey: .mothersMaidenName) ?? ""
        countryOfBirth = try applicantsValues.decodeIfPresent(String.self, forKey: .countryOfBirth) ?? ""
        townOfBirth = try applicantsValues.decodeIfPresent(String.self, forKey: .townOfBirth) ?? ""
        previousLastName = try applicantsValues.decodeIfPresent(String.self, forKey: .previousLastName) ?? ""
        addresses = try applicantsValues.decodeIfPresent([OnfidoAddress].self, forKey: .addresses) ?? []
        
    }
    
    
    
}

