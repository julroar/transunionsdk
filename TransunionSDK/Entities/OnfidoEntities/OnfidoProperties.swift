//
//  OnfidoProperties.swift
//  onfidoTest
//
//  Created by USER on 7/15/19.
//  Copyright © 2019 david. All rights reserved.
//

import Foundation

struct OnfidoProperties:Codable{
    var nationality:String?
    var lastName:String?
    var issuingCountry:String?
    var gender:String?
    var firstName:String?
    var documentType:String?
    var documentNumbers:String?
    var dateOfExpiry:String?
    var dateOfBirth:String?
    var score:Float?
    
    enum codingKeys: String, CodingKey{
        case nationality = "nationality"
        case lastName = "last_name"
        case issuingCountry = "issuing_country"
        case gender = "gender"
        case firstName = "first_name"
        case documentType = "document_type"
        case documentNumbers = "document_numbers"
        case dateOfExpiry = "date_of_expiry"
        case dateOfBirth = "date_of_birth"
        case score = "score"
    }
    
    public init(from decoder:Decoder) throws{
        let propertyValues = try decoder.container(keyedBy: codingKeys.self)
        nationality = try propertyValues.decodeIfPresent(String.self, forKey: .nationality)
        lastName = try propertyValues.decodeIfPresent(String.self, forKey: .lastName)
        issuingCountry = try propertyValues.decodeIfPresent(String.self, forKey: .issuingCountry)
        gender = try propertyValues.decodeIfPresent(String.self, forKey: .gender)
        firstName = try propertyValues.decodeIfPresent(String.self, forKey: .firstName)
        documentType = try propertyValues.decodeIfPresent(String.self, forKey: .documentType)
        documentNumbers = try propertyValues.decodeIfPresent(String.self, forKey: .documentNumbers)
        dateOfExpiry = try propertyValues.decodeIfPresent(String.self, forKey: .dateOfExpiry)
        dateOfBirth = try propertyValues.decodeIfPresent(String.self, forKey: .dateOfBirth)
        score = try propertyValues.decodeIfPresent(Float.self, forKey: .score)
    }
}
