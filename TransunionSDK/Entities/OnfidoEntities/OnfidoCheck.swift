//
//  OnfidoCheck.swift
//  onfidoTest
//
//  Created by USER on 7/13/19.
//  Copyright © 2019 david. All rights reserved.
//

import Foundation

struct OnfidoCheck: Codable{
    var type:String?
    var reports:[OnfidoReport]?
    
    
    
    enum CodingKeys: String, CodingKey {
        case type = "type"
        case reports = "reports"
    }
    
    init(){
        
    }
    
    func encode(to encoder: Encoder) throws{
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(type, forKey: .type)
        try container.encodeIfPresent(reports, forKey: .reports)
    }
}
