//
//  OnfidoAddress.swift
//  onfidoTest
//
//  Created by USER on 7/15/19.
//  Copyright © 2019 david. All rights reserved.
//

import Foundation

struct OnfidoAddress:Codable {
    var flatNumber:String?
    var buildingNumber:String?
    var buildingName:String?
    var street:String?
    var subStreet:String?
    var town:String?
    var state:String?
    var postcode:String?
    var country:String?
    var startDate:String?
    var endDate:String?
    
    enum CodingKeys: String, CodingKey{
        case flatNumber = "flat_number"
        case buildingNumber = "building_number"
        case buildingName = "building_name"
        case street = "street"
        case subStreet = "sub_street"
        case town = "town"
        case state = "state"
        case postcode = "postcode"
        case country = "country"
        case startDate = "start_date"
        case endDate = "end_date"
    }
    
    public init(from decoder: Decoder) throws {
        
        let addressValues = try decoder.container(keyedBy: CodingKeys.self)
        flatNumber = try addressValues.decodeIfPresent(String.self, forKey: .flatNumber) ?? ""
        buildingNumber = try addressValues.decodeIfPresent(String.self, forKey: .buildingNumber) ?? ""
        buildingName = try addressValues.decodeIfPresent(String.self, forKey: .buildingName) ?? ""
        street = try addressValues.decodeIfPresent(String.self, forKey: .street) ?? ""
        subStreet = try addressValues.decodeIfPresent(String.self, forKey: .subStreet) ?? ""
        town = try addressValues.decodeIfPresent(String.self, forKey: .town) ?? ""
        state = try addressValues.decodeIfPresent(String.self, forKey: .state) ?? ""
        postcode = try addressValues.decodeIfPresent(String.self, forKey: .postcode) ?? ""
        country = try addressValues.decodeIfPresent(String.self, forKey: .country) ?? ""
        startDate = try addressValues.decodeIfPresent(String.self, forKey: .startDate) ?? ""
        endDate = try addressValues.decodeIfPresent(String.self, forKey: .endDate) ?? ""
        
    }
    
}
