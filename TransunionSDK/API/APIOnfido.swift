//
//  APIOnfido.swift
//  onfidoTest
//
//  Created by USER on 7/16/19.
//  Copyright © 2019 david. All rights reserved.
//

import Foundation
import UIKit

class APIOnfido {
    
    
    static func createApplicant(applicant: OnfidoApplicants, completion:@escaping(HTTPURLResponse,Result<OnfidoApplicants>)->Void) {
        print(applicant)
        URLSession.shared.dataTask(with:APIRouter.applicant(applicant: applicant).asURLRequest()) { data, response, error in
            print(response)
            print(error)
            guard let data = data, error == nil, let response = response as? HTTPURLResponse else{
                return
            }
            let datas = String(bytes: data, encoding: String.Encoding.utf8)
            print(datas)
            let decoder = JSONDecoder()
            guard   let serverResponse = try? decoder.decode(OnfidoApplicants.self, from: data) else{
                completion( response,  Result<OnfidoApplicants>.failure(error) )
                return
            }
            completion( response, Result<OnfidoApplicants>.success(serverResponse))
        }.resume()
    }
    
    static func onfidoCheck(onfidoCheck:OnfidoCheck, applicant:OnfidoApplicants, completion:@escaping(HTTPURLResponse, Result<OnfidoCheckResponse>)->Void){
        
        URLSession.shared.dataTask(with:APIRouter.onfidoCheck(onfidoCheck: onfidoCheck, applicant: applicant).asURLRequest()) { data, response, error in
            
            guard let data = data, error == nil, let response = response as? HTTPURLResponse else{
                return
            }
            let decoder = JSONDecoder()
            guard   let serverResponse = try? decoder.decode(OnfidoCheckResponse.self, from: data) else{
                completion( response,  Result<OnfidoCheckResponse>.failure(error) )
                return
            }
            completion( response, Result<OnfidoCheckResponse>.success(serverResponse))
            }.resume()
    }
    
}
