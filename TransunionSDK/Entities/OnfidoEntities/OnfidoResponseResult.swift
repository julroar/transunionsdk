//
//  OnfidoResult.swift
//  onfidoTest
//
//  Created by USER on 7/15/19.
//  Copyright © 2019 david. All rights reserved.
//

import Foundation

struct OnfidoResponseResult:Codable{
    var result:String?
    
    enum codingKeys:String, CodingKey {
        case result = "result"
    }
    
    public init(from decoder:Decoder) throws{
        let resultValues = try decoder.container(keyedBy: codingKeys.self)
        result = try resultValues.decodeIfPresent(String.self, forKey: .result)
    }
}
