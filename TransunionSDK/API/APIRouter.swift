//
//  APIRouter.swift
//  onfidoTest
//
//  Created by USER on 7/15/19.
//  Copyright © 2019 david. All rights reserved.
//

import Foundation
import UIKit

enum APIRouter: URLRequestConvertible {
    
    case applicant(applicant:OnfidoApplicants)
    case onfidoCheck(onfidoCheck:OnfidoCheck , applicant:OnfidoApplicants)
    case onfidoDownloadDocument(applicant:OnfidoApplicants, idDocument:String)
    case onfidoDownloadSelfie(idPhoto:String)
    
    
    // MARK: - HTTPMethod
    private var method: HTTPMethod {
        switch self {
        case .applicant, .onfidoCheck :
            return .post
            
        case .onfidoDownloadSelfie, .onfidoDownloadDocument :
            return .get
        /*case :
            return .put
            
        case :
            return .delete
            
        case :
            return .put*/
        }
    }
    
    // MARK: - Path
    private var path: String {
        switch self {
            
        case .applicant:
            return URLK.onfidoApplicants.rawValue
        case .onfidoCheck(_, let applicant):
            var urlPath = URLK.onfidoApplicants.rawValue
            urlPath = urlPath.replacingOccurrences(of: ":applicant_id", with: applicant.id ?? "")
            return urlPath
        case .onfidoDownloadSelfie(let idPhoto):
            var urlPath = URLK.onfidoDownloadSelfie.rawValue
            urlPath = urlPath.replacingOccurrences(of: ":live_photo_id", with: idPhoto)
            return urlPath
        case .onfidoDownloadDocument(let applicant, let idDocument):
            var urlPath = URLK.onfidoDownloadDocument.rawValue
            urlPath = urlPath.replacingOccurrences(of: ":applicant_id", with: applicant.id ?? "")
            urlPath = urlPath.replacingOccurrences(of: ":document_id", with:  idDocument)
            return urlPath
        }
    }
    
    // MARK: - Parameters
    private var parameters: Parameters? {
        switch self {
        case .applicant(let applicant):
            return applicant.dictionary
        case .onfidoCheck(let check, _):
            return check.dictionary
        case .onfidoDownloadDocument:
            return nil
        case .onfidoDownloadSelfie:
            return nil
        }
    }
    
    
    
    // MARK: - URLRequestConvertible
    func asURLRequest() -> URLRequest {
        
        
        var url:URL
        
        switch self {
        case .applicant, .onfidoCheck, .onfidoDownloadDocument, .onfidoDownloadSelfie:
            url =  Configuration.urlBaseOnfido.asURL()!
        default:
            url = Configuration.urlBaseTransUnion.asURL()!
        }
        

        var urlRequest:URLRequest
        
        let authToken = Configuration.tokenSDK
        let token = ContentType.Token.rawValue + authToken
        //let authBearer = UserDefaults.standard.getAccessBearer()
        //let bearer = ContentType.Bearer.rawValue + authBearer
        urlRequest = URLRequest(url: url.appendingPathComponent(path))
        // HTTP Method
        urlRequest.httpMethod = method.rawValue
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
        urlRequest.setValue(token, forHTTPHeaderField: HTTPHeaderField.authorization.rawValue)
        
        switch self{
            
            

        /*case :
            
            url = returnUrl(path: path)
            url = url.appendingPathComponent(path)
            let urlAll =  url.absoluteString
            let parameters =  self.parameters!.stringFromHttpParameters()
            
            
            var urlComponents =  URLComponents(string: urlAll)
            
            urlComponents?.query  =  parameters.removingPercentEncoding
            let urlEnrollment = urlComponents?.url
            urlRequest = URLRequest(url: urlEnrollment!)
            
            
            // HTTP Method
            urlRequest.httpMethod = method.rawValue
            // Common Headers
            urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)

            urlRequest.setValue(token , forHTTPHeaderField: HTTPHeaderField.authorization.rawValue)



            
            return urlRequest
            
       */
            
        default:
            if let parameters = parameters {
                do {
                    urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
                } catch let error{
                    print(error)
                }
            }
        }
        
        return urlRequest
    }
}
