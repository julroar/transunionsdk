//
//  OnfidoReports.swift
//  onfidoTest
//
//  Created by USER on 7/15/19.
//  Copyright © 2019 david. All rights reserved.
//

import Foundation
struct OnfidoReports:Codable{
    var id:String?
    var name:String?
    var createdAt:String?
    var href:String?
    var result:String?
    var status:String?
    var subResult:String?
    var variant:String?
    var properties:OnfidoProperties?
    var breakdown:OnfidoBreakdown?
    
    enum codingKeys:String, CodingKey{
        case id = "id"
        case name = "name"
        case createdAt = "created_at"
        case href = "href"
        case result = "result"
        case status = "status"
        case subResult = "subResult"
        case variant = "variant"
        case properties = "properties"
        case breakdown = "breakdown"
    }
    
    public init(from decoder:Decoder) throws{
        let reportValues = try decoder.container(keyedBy: codingKeys.self)
        id = try reportValues.decodeIfPresent(String.self, forKey: .id) ?? ""
        name = try reportValues.decodeIfPresent(String.self, forKey: .name) ?? ""
        createdAt = try reportValues.decodeIfPresent(String.self, forKey: .createdAt) ?? ""
        href = try reportValues.decodeIfPresent(String.self, forKey: .href) ?? ""
        result = try reportValues.decodeIfPresent(String.self, forKey: .result) ?? ""
        status = try reportValues.decodeIfPresent(String.self, forKey: .status) ?? ""
        subResult = try reportValues.decodeIfPresent(String.self, forKey: .subResult) ?? ""
        variant = try reportValues.decodeIfPresent(String.self, forKey: .variant) ?? ""
        properties = try reportValues.decodeIfPresent(OnfidoProperties.self, forKey: .properties)
        breakdown = try reportValues.decodeIfPresent(OnfidoBreakdown.self, forKey: .id)
    }
}
