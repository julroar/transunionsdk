//
//  Configurations.swift
//  onfidoTest
//
//  Created by USER on 7/15/19.
//  Copyright © 2019 david. All rights reserved.
//

import Foundation
import UIKit


struct Configuration {
    
    
    static var tokenSDK = ""
    static var lastNameTransunion = ""
    static var nameTransUnion = ""
    static var urlBaseTransUnion = ""
    static var urlBaseOnfido = ""
    
    
}

enum pList:String{
    
    case Info = "Info"
    case ConfigDevelopment = "ConfigDevelopment"
    case ConfigTest = "ConfigTest"
    case ConfigProduction = "ConfigProduction"
    case ServerType = "ServerType"
}

enum ServerType:Int{
    case development = 1
    case test = 2
    case production = 3
}

enum configurationKeys:String{
    
    case tokenSDK = "tokenSDK"
    case lastNameTransunion = "lastNameTransunion"
    case nameTransUnion = "nameTransUnion"
    case urlBaseTransUnion = "urlBaseTransUnion"
    case urlBaseOnfido = "urlBaseOnfido"
    
}
