//
//  APIConstant.swift
//  onfidoTest
//
//  Created by USER on 7/15/19.
//  Copyright © 2019 david. All rights reserved.
//

import UIKit


struct K {
    
    
    struct APIParameterKey {
       
    }
    
}

enum URLK:String{
    
    //Onfido
    case onfidoApplicants = "/v2/applicants"
    case onfidoCheck = "/v2/applicants/:applicant_id/checks"
    case onfidoDownloadDocument = "/v2/applicants/:applicant_id/documents/:document_id/download"
    case onfidoDownloadSelfie = "/v2/live_photos/:live_photo_id/download"
    //Transunion
    
    
}

enum HTTPHeaderField: String {
    case authorization = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "accept"
    case acceptEncoding = "Accept-Encoding"
    
}

enum ContentType: String {
    case oauth = "application/x-www-form-urlencoded"
    case json = "application/json"
    case Bearer = "Bearer "
    case Token = "Token token="
}


public enum HTTPMethod: String {
    case options = "OPTIONS"
    case get     = "GET"
    case head    = "HEAD"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
    case trace   = "TRACE"
    case connect = "CONNECT"
}

/// A dictionary of parameters to apply to a `URLRequest`.
public typealias Parameters = [String: Any]

public protocol URLRequestConvertible {
    /// Returns a URL request or throws if an `Error` was encountered.
    ///
    /// - throws: An `Error` if the underlying `URLRequest` is `nil`.
    ///
    /// - returns: A URL request.
    func asURLRequest() -> URLRequest
}

extension String: URLConvertible {
    /// Returns a URL if `self` represents a valid URL string that conforms to RFC 2396 or throws an `AFError`.
    ///
    /// - throws: An `AFError.invalidURL` if `self` is not a valid URL string.
    ///
    /// - returns: A URL or throws an `AFError`.
    public func asURL() -> URL? {
        guard let url = URL(string: self) else { return URL(string: "") }
        return url
    }
}

public protocol URLConvertible {
    /// Returns a URL that conforms to RFC 2396 or throws an `Error`.
    ///
    /// - throws: An `Error` if the type cannot be converted to a `URL`.
    ///
    /// - returns: A URL or throws an `Error`.
    func asURL() -> URL?
}


public enum Result<Value> {
    case success(Value)
    case failure(Error?)
    
    /// Returns `true` if the result is a success, `false` otherwise.
    public var isSuccess: Bool {
        switch self {
        case .success:
            return true
        case .failure:
            return false
        }
    }
    
    /// Returns `true` if the result is a failure, `false` otherwise.
    public var isFailure: Bool {
        return !isSuccess
    }
    
    /// Returns the associated value if the result is a success, `nil` otherwise.
    public var value: Value? {
        switch self {
        case .success(let value):
            return value
        case .failure:
            return nil
        }
    }
    
    /// Returns the associated error value if the result is a failure, `nil` otherwise.
    public var error: Error? {
        switch self {
        case .success:
            return nil
        case .failure(let error):
            return error
        }
    }
}
