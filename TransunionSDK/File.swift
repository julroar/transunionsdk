//
//  File.swift
//  test1
//
//  Created by Julian Arredondo on 7/17/19.
//  Copyright © 2019 Julian Arredondo. All rights reserved.
//

import Foundation
open class SubFrameworkHome: NSObject {
    open class func sayHello() {
        print("Saying hello from Subframework")
        
    }
}
